package com.xujiaji.learnopengl.main;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Triangle {

    private FloatBuffer vertexBuffer;
    private int mProgram;
    private int mPositionHandle;
    private int mColorHandle;

    // số lượng tọa độ trên mỗi đỉnh trong mảng này
    static final int COORDS_PER_VERTEX = 3;

    static float triangleCords[] = {   // theo thứ tự ngược chiều kim đồng hồ:
            -0.3963623f, 0.5942515f, 0f, // top
            0.51104736f, 0.12319994f, 0f, // bottom left
            -0.28710938f, -0.1701374f, 0f// bottom right
    };

    // Đặt màu với các giá trị đỏ, lục, lam và alpha (độ mờ)
    float color[] = {0.3f, 0.3f, 0.3f, 1.0f};


    private final String vertexShaderCode =
            "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = vPosition;" +
                    "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";


    public Triangle() {
        int vertexShader = RendererWrapper.loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = RendererWrapper.loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLES20.glCreateProgram();             // tạo Chương trình OpenGL ES trống
        GLES20.glAttachShader(mProgram, vertexShader);   // thêm công cụ đổ bóng đỉnh vào chương trình
        GLES20.glAttachShader(mProgram, fragmentShader); // thêm trình đổ bóng phân mảnh vào chương trình
        GLES20.glLinkProgram(mProgram);                  // tạo các tệp thực thi chương trình OpenGL ES

        // khởi tạo bộ đệm byte đỉnh cho tọa độ hình dạng
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (số lượng giá trị tọa độ * 4 byte trên mỗi float)
                triangleCords.length * 4);
        // sử dụng thứ tự byte gốc của phần cứng thiết bị
        bb.order(ByteOrder.nativeOrder());

        // tạo bộ đệm dấu chấm động từ ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // thêm tọa độ vào FloatBuffer
        vertexBuffer.put(triangleCords);
        // đặt bộ đệm để đọc tọa độ đầu tiên
        vertexBuffer.position(0);
    }

    public void draw() {
        // Thêm chương trình vào môi trường OpenGL ES
        GLES20.glUseProgram(mProgram);

        // nhận xử lý thành viên vPosition của vertex shader
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        // Cho phép một chốt đối với đỉnh tam giác
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Chuẩn bị dữ liệu tọa độ tam giác
        int vertexStride = COORDS_PER_VERTEX * 4;
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        // nhận xử lý thành viên vColor của phân mảnh shader
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        // Đặt màu để vẽ hình tam giác
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);

        // Vẽ hình tam giác
        int vertexCount = triangleCords.length / COORDS_PER_VERTEX;
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Tắt mảng đỉnh
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}
