package com.xujiaji.learnopengl.main;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/*Renderer: là một interface định nghĩa các methods được yêu cầu để vẽ đồ họa trên GlSurfaceVew*/

public class RendererWrapper implements GLSurfaceView.Renderer {
    private Triangle mTriangle;

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        // Set the background frame color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        // initialize a triangle
        mTriangle = new Triangle();
    }

    //Hệ thống gọi method này khi GlSurfaceView thay đổi kích thước hoặc khi xoay thiết bị
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    //Hệ thống gọi method này mỗi lần vẽ lại GlSurfaceView
    @Override
    public void onDrawFrame(GL10 unused) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        mTriangle.draw();
    }

    public static int loadShader(int type, String shaderCode) {
        // tạo kiểu đổ bóng đỉnh (GLES20.GL_VERTEX_SHADER)
        // hoặc một loại đổ bóng phân mảnh (GLES20.GL_FRAGMENT_SHADER)

        int shader = GLES20.glCreateShader(type);

        // thêm mã nguồn vào shader và biên dịch nó
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }
}

